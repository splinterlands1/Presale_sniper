﻿using HiveAPI.CS;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace BuyDemo
{
    internal class Program
    {
        /*
         Splinterlands Auto Buy Program
         Written By : @chaoscommander

        The purpose of this program is to demonstrate how to automate purchasing directly from the blockchain.
        With a little understanding of what is going on this program can be modified to suit other presales.
        It is my hope that this will lead to changes in the system to prevent abuse.
        
        This program does work, however please do not use it.
        Push and shove responsibly.         
        */

        #region Variables
        private static HttpClient oHTTP = new HttpClient();                     //This is the component for connecting to stuff on the internet
        private static CHived oHived = new CHived(oHTTP, "https://anyx.io");    //This is the component used to interact with the blockchain
        private static string hive_user_1 = "";                                 //Variable for storing hive username
        private static string hive_activekey_1 = "";                            //Variable for storing hive active key
        private static string eth_address = "";                                 //Variable for storing eth address
        #endregion

        static async Task Main(string[] args)
        {
            //This code loads the information located in the settings.ini file
            var Settings = new IniFile("Settings.ini");
            hive_user_1 = Settings.Read("Hive_Account");
            eth_address = Settings.Read("ETH_Address");
            hive_activekey_1 = Settings.Read("Hive_Active_Key");
            DateTime target = DateTime.Parse(Settings.Read("Datetime"));

            DateTime now = DateTime.Now;
            Console.Clear();
            Console.CursorVisible = false;
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(" Runi Presale Sniper");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(" USE AT OWN RISK.  NOT RESPONSIBLE FOR LOSSES");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine();
            Console.WriteLine(" The Countdown should match the Website.");
            Console.WriteLine(" If not adjust the Settings.ini file.");
            Console.WriteLine();
            Console.WriteLine(" Hive Account : " + hive_user_1.ToLower());
            Console.WriteLine(" ETH Address  : " + eth_address);
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Red;

            //Parse our custom_json first this could be modified to send any custom_json.
            //This information can be exposed from the splinterlands website by setting your clock forward.
            //Hopefully we see changes that prevent this from happening in the future.
            COperations.custom_json custom_json = new COperations.custom_json
            {
                id = "sm_purchase",
                json = "{ \"id\":\"runi_components_kit\",\"qty\":1,\"eth_address\":\"" + eth_address + "\",\"app\":\"presale_sniper/1.0.002\",\"n\":\"" + randomN() + "\"}",
                required_auths = new string[] { hive_user_1.ToLower() },
                required_posting_auths = new string[] { }
            };

            //Now lets create the arrays needed for the blockchain post.  We do this and the custom_json beforehand as it is slightly faster.
            object[] c_0 = { custom_json };
            string[] key = { hive_activekey_1 };


            //This is the main program loop.  We get the current time then we take the time from the settings.ini file and figure out time until purchase.
            //Once this is past we fall through the loop.          
            do
            {
                now = DateTime.Now;
                TimeSpan data = target - now;
                Console.SetCursorPosition(0, 9);
                Console.Write(" Time Until Purchase [ " + data.ToString().Substring(0, data.ToString().LastIndexOf(".")) + " ]");
            } while (target > now);

            Console.WriteLine();
            Console.WriteLine();


            //Post to blockchain.
            try
            {
                string txid = oHived.broadcast_transaction(c_0, key);  //here is where the magic happens.  The block is transmitted to the chain.
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(" " + txid);
            }
            catch (Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(" Failed");
            }
            Console.ReadLine();
        }

        private static string randomN()
        {
            //This function produces a random chain of letters 10 characters long for the transaction.
            Random ran = new Random();
            String b = "abcdefghijklmnopqrstuvwxyz0123456789";
            int length = 10;
            String random = "";
            for (int i = 0; i < length; i++)
            {
                int a = ran.Next(36);
                random = random + b.ElementAt(a);
            }
            return random;
        }
    }
}
