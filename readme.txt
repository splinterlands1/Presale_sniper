This is inteneded for educational purposes only.
You will need to have visual studio installed on your computer.
It can be downloaded for free from Microsoft's website.
to open the program and look at the code open BuyDemo.sln

To run the program go to the Bin/Debug folder
and open settings.ini

you will have to replace the hive user / active key / eth_address / date
with your information.

then run BuyDemo.exe

Obviously you will not be getting a runi as the sale is over.
But you can still send transactions to try it out.

This code would need modification to work with other sales.  
Since I do not condone the use of automated programs for presales I will not be providing the modifications.
I will be doing all of my own purchases manually.

My hope is by releasing this code I will drive positive change and an improved user experiance for all.

If you have any questions feel free to reach out to me on the Splinterlands Discord
I am CommanderChaos#7729